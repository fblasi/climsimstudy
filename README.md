R-code of the manuscript "A selective view of climatological data and likelihood estimation"

The R-code was developed and ran using the following software:

**R-version:** 4.1.1
**Linux distro:** Ubuntu 20.04 LTS

steps to build and run the docker container:

1 - git clone https://git.math.uzh.ch/fblasi/climsimstudy.git

2 - docker build . -t climsimstudy

3 - docker run --rm -p 8787:8787 -v (path to the climsimstudy folder):/home/rstudio climsimstudy

4 - open the web browser and go to localhost:8787

you may need root access for steps 2 and 3.